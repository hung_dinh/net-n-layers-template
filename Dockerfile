FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["src/BubbleTea.Api/BubbleTea.Api.csproj", "BubbleTea.Api/"]
COPY ["src/BubbleTea.Service/BubbleTea.Service.csproj", "BubbleTea.Service/"]
COPY ["src/BubbleTea.DataAccess/BubbleTea.DataAccess.csproj", "BubbleTea.DataAccess/"]
COPY ["src/BubbleTea.Core/BubbleTea.Core.csproj", "BubbleTea.Core/"]

RUN dotnet restore "BubbleTea.Api/BubbleTea.Api.csproj"
COPY src/. .
WORKDIR "/src/BubbleTea.Api"

RUN ls -la
RUN dotnet build "BubbleTea.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "BubbleTea.Api.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .

ENTRYPOINT ["dotnet", "BubbleTea.Api.dll"]