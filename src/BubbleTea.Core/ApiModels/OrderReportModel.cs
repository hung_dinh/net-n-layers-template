﻿namespace BubbleTea.Core.ApiModels
{
    public class OrderReportModel
    {
        public string StoreNumer { get; set; }
        public decimal OrderPriceSum { get; set; }
        public long OrderTotal { get; set; }
    }
}
