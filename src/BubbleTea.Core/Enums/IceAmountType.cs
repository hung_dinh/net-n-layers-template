﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbleTea.Core.Enums
{
    public enum IceAmountType
    {
        None = 0,
        Half = 1,
        Full = 2
    }
}
