﻿using BubbleTea.DataAccess.DbContexts;
using BubbleTea.DataAccess.Interfaces;
using BubbleTea.DataAccess.Models;
using System.Threading.Tasks;

namespace BubbleTea.DataAccess.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(BubbleTeaDbContext dbContext
            , IOrderRepository orderRepository
            , IRepository<OrderDetail> orderDetailRepository
            , IRepository<OrderToppingDetail> orderToppingDetailRepository
            , IRepository<Flavour> flavourRepository
            , IRepository<Topping> toppingRepository
            , IRepository<FlavourTopping> flavourToppingRepository
            )
        {
            _dbContext = dbContext;
            OrderRepository = orderRepository;
            OrderDetailRepository = orderDetailRepository;
            OrderToppingDetailRepository = orderToppingDetailRepository;
            FlavourRepository = flavourRepository;
            ToppingRepository = toppingRepository;
            FlavourToppingRepository = flavourToppingRepository;
        }

        private readonly BubbleTeaDbContext _dbContext;
        public IOrderRepository OrderRepository { get; set; }
        public IRepository<OrderDetail> OrderDetailRepository { get; set; }
       public IRepository<OrderToppingDetail> OrderToppingDetailRepository { get; set; }
        public IRepository<Topping> ToppingRepository { get; set; }
        public IRepository<Flavour> FlavourRepository { get; set; }
        public IRepository<FlavourTopping> FlavourToppingRepository { get; set; }

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
