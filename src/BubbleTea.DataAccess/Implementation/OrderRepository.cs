﻿using BubbleTea.Core.ApiModels;
using BubbleTea.DataAccess.DbContexts;
using BubbleTea.DataAccess.Interfaces;
using BubbleTea.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbleTea.DataAccess.Implementation
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(BubbleTeaDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<OrderReportModel>> GetReportAsync(DateTime fromDate, DateTime toDate)
        {
            var query = _dbContext.Orders.Where(o => o.OrderDateTime >= fromDate && o.OrderDateTime < toDate);
            return await query.GroupBy(o => o.StoreNumber).Select(g => new OrderReportModel
            {
                StoreNumer = g.Key,
                OrderTotal = g.Count(),
                OrderPriceSum = g.Sum(o => o.TotalPrice)
            }).ToListAsync();
        }
    }
}
