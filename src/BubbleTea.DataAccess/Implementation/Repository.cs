﻿using BubbleTea.DataAccess.DbContexts;
using BubbleTea.DataAccess.Interfaces;
using BubbleTea.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BubbleTea.DataAccess.Implementation
{
    public class Repository<T> : IRepository<T> where T : BaseTable
    {
        protected readonly BubbleTeaDbContext _dbContext;

        public Repository(BubbleTeaDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<T>> FilterAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbContext.Set<T>().Where(predicate).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetListAsync()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }
        public async Task InsertAsync(T obj)
        {
            await _dbContext.Set<T>().AddAsync(obj);
        }
    }
}
