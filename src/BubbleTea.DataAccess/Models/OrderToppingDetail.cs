﻿using System.ComponentModel.DataAnnotations;

namespace BubbleTea.DataAccess.Models
{
    public class OrderToppingDetail : BaseTable
    {
        [Required]
        public int OrderDetailId { get; set; }

        [Required]
        public int ToppingId { get; set; }

        public virtual OrderDetail OrderDetail { get; set; }
        public virtual Topping Topping { get; set; }
    }
}
