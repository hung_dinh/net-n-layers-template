﻿using BubbleTea.Core.Enums;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BubbleTea.DataAccess.Models
{
    public class OrderDetail : BaseTable
    {
        public OrderDetail()
        {
            OrderToppingDetails = new HashSet<OrderToppingDetail>();
        }

        [Required]
        public int OrderId { get; set; }

        [Required]
        public int FlavourId { get; set; }

        [Required]
        public int Quantity { get; set; } = 1;

        [Required]
        public IceAmountType AmountOfIce { get; set; } = IceAmountType.None;

        public virtual Flavour Flavour { get; set; }
        public virtual Order Order { get; set; }
        public virtual ICollection<OrderToppingDetail> OrderToppingDetails { get; set; }
    }
}
