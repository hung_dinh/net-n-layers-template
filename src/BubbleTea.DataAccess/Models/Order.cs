﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BubbleTea.DataAccess.Models
{
    public class Order : BaseTable
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        [Required, StringLength(50)]
        public string StoreNumber { get; set; }

        [Required, StringLength(50)]
        public string OrderNumber { get; set; }

        public DateTime OrderDateTime { get; set; } = DateTime.UtcNow;

        public decimal TotalPrice { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
