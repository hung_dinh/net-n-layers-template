﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BubbleTea.DataAccess.Models
{
    public class Topping : BaseTable
    {
        public Topping()
        {
            FlavourToppings = new HashSet<FlavourTopping>();
            OrderToppingDetails = new HashSet<OrderToppingDetail>();
        }

        [Required, StringLength(256)]
        public string Name { get; set; }

        public virtual ICollection<FlavourTopping> FlavourToppings { get; set; }
        public virtual ICollection<OrderToppingDetail> OrderToppingDetails { get; set; }
    }
}
