﻿using BubbleTea.Core.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BubbleTea.DataAccess.Models
{
    public class Flavour : BaseTable
    {
        public Flavour()
        {
            FlavourToppings = new HashSet<FlavourTopping>();
            OrderDetails = new HashSet<OrderDetail>();
        }

        [Required, StringLength(256)]
        public string Name { get; set; }

        public IceAmountType? AmountOfIce { get; set; }

        public virtual ICollection<FlavourTopping> FlavourToppings { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
