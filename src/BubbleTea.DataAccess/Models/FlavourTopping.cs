﻿using System.ComponentModel.DataAnnotations;

namespace BubbleTea.DataAccess.Models
{
    public class FlavourTopping : BaseTable
    {
        [Required]
        public int FlavourId { get; set; }

        [Required]
        public int ToppingId { get; set; }

        public virtual Flavour Flavour { get; set; }
        public virtual Topping Topping { get; set; }
    }
}
