﻿using BubbleTea.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbleTea.DataAccess.Interfaces
{
    public interface IUnitOfWork
    {
        IOrderRepository OrderRepository { get; set; }
        IRepository<OrderDetail> OrderDetailRepository { get; set; }
        IRepository<OrderToppingDetail> OrderToppingDetailRepository { get; set; }
        IRepository<Topping> ToppingRepository { get; set; }
        IRepository<Flavour> FlavourRepository { get; set; }
        IRepository<FlavourTopping> FlavourToppingRepository { get; set; }

        Task SaveChangesAsync();
    }
}
