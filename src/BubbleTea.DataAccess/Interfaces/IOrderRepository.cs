﻿using BubbleTea.Core.ApiModels;
using BubbleTea.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BubbleTea.DataAccess.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        Task<List<OrderReportModel>> GetReportAsync(DateTime fromDate, DateTime toDate);
    }
}
