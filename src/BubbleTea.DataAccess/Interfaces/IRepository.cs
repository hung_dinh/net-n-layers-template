﻿using BubbleTea.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BubbleTea.DataAccess.Interfaces
{
    public interface IRepository<T> where T : BaseTable
    {
        Task<IEnumerable<T>> FilterAsync(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> GetListAsync();
        Task InsertAsync(T obj);
    }
}
