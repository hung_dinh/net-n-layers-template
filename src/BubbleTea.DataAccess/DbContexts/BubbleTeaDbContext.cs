﻿using BubbleTea.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BubbleTea.DataAccess.DbContexts
{
    public class BubbleTeaDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseInMemoryDatabase("BubbleTea");
        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Flavour> Flavours { get; set; }
        public DbSet<Topping> Toppings { get; set; }
        public DbSet<FlavourTopping> FlavourToppings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.HasOne(d => d.Order)
                    .WithMany(o => o.OrderDetails)
                    .HasForeignKey(od => od.OrderId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.Flavour)
                    .WithMany(f => f.OrderDetails)
                    .HasForeignKey(od => od.FlavourId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<FlavourTopping>(entity =>
            {
                entity.HasOne(ft => ft.Flavour)
                   .WithMany(f => f.FlavourToppings)
                   .HasForeignKey(od => od.FlavourId)
                   .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(ft => ft.Topping)
                    .WithMany(t => t.FlavourToppings)
                    .HasForeignKey(d => d.ToppingId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<OrderToppingDetail>(entity =>
            {
                entity.HasOne(ft => ft.OrderDetail)
                   .WithMany(f => f.OrderToppingDetails)
                   .HasForeignKey(od => od.OrderDetailId)
                   .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(ft => ft.Topping)
                    .WithMany(t => t.OrderToppingDetails)
                    .HasForeignKey(d => d.ToppingId)
                    .OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}
