﻿using BubbleTea.DataAccess.DbContexts;
using BubbleTea.DataAccess.Models;
using System.Collections.Generic;
using System.Linq;

namespace BubbleTea.DataAccess
{
    public static class SeedingData
    {
        public static void Initiate(BubbleTeaDbContext dbContext)
        {
            dbContext.Database.EnsureCreated();

            dbContext.Flavours.RemoveRange(dbContext.Flavours.ToList());
            dbContext.Toppings.RemoveRange(dbContext.Toppings.ToList());
            dbContext.FlavourToppings.RemoveRange(dbContext.FlavourToppings.ToList());

            dbContext.Flavours.AddRange(new List<Flavour> {
                new Flavour { Id = 1, Name = "Milk Tea" },
                new Flavour { Id = 2, Name = "Premium Milk Tea" },
                new Flavour { Id = 3, Name = "Lychee" },
                new Flavour { Id = 4, Name = "Brown Sugar", AmountOfIce = Core.Enums.IceAmountType.Full },
            });

            dbContext.Toppings.AddRange(new List<Topping> {
                new Topping { Id = 1, Name = "Tapioca Pearls" },
                new Topping { Id = 2, Name = "Jelly" },
                new Topping { Id = 3, Name = "Cream Top" },
                new Topping { Id = 4, Name = "Oreo" },
            });

            dbContext.FlavourToppings.AddRange(new List<FlavourTopping> {
                new FlavourTopping { Id = 1, FlavourId = 1, ToppingId = 1 },
                new FlavourTopping { Id = 2, FlavourId = 1, ToppingId = 2 },
                new FlavourTopping { Id = 3, FlavourId = 1, ToppingId = 3 },
                new FlavourTopping { Id = 4, FlavourId = 1, ToppingId = 4 },
                new FlavourTopping { Id = 5, FlavourId = 2, ToppingId = 1 },
                new FlavourTopping { Id = 6, FlavourId = 2, ToppingId = 2 },
                new FlavourTopping { Id = 7, FlavourId = 2, ToppingId = 3 },
                new FlavourTopping { Id = 8, FlavourId = 2, ToppingId = 4 },
                new FlavourTopping { Id = 9, FlavourId = 3, ToppingId = 1 },
                new FlavourTopping { Id = 10, FlavourId = 3, ToppingId = 2 },
                new FlavourTopping { Id = 11, FlavourId = 3, ToppingId = 3 },
                new FlavourTopping { Id = 12, FlavourId = 3, ToppingId = 4 },
                new FlavourTopping { Id = 13, FlavourId = 4, ToppingId = 1 },
            });

            dbContext.SaveChanges();
        }
    }
}
