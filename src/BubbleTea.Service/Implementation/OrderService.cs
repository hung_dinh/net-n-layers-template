﻿using BubbleTea.Core.ApiModels;
using BubbleTea.Core.Enums;
using BubbleTea.DataAccess.Interfaces;
using BubbleTea.DataAccess.Models;
using BubbleTea.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace BubbleTea.Service.Implementation
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        public OrderService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<string>> CreateOrder(OrderCreationModel model)
        {
            var errors = new List<string>();

            var flavourIds = model.Flavours.Select(f => f.FlavourId).Distinct().ToList();
            var flavours = await _unitOfWork.FlavourRepository.FilterAsync(f => flavourIds.Contains(f.Id));

            flavourIds.ForEach(fid =>
            {
                if (!flavours.Any(f => f.Id == fid))
                    errors.Add($"Flavour with Id {fid} does not exist");
            });

            var toppingIds = model.Flavours.SelectMany(f => f.ToppingIds ?? new List<int>()).Distinct().ToList();
            var toppings = await _unitOfWork.ToppingRepository.FilterAsync(t => toppingIds.Contains(t.Id));

            toppingIds.ForEach(tid =>
            {
                if (!toppings.Any(t => t.Id == tid))
                    errors.Add($"Topping with Id {tid} does not exist");
            });

            var iceAmountLevels = model.Flavours.Select(f => f.AmountOfIce).Distinct().ToList();
            iceAmountLevels.ForEach(l =>
            {
                if (!Enum.IsDefined(typeof(IceAmountType), (int)l))
                    errors.Add($"Ice Amonut with value {(int)l} does not exist");
            });

            var order = new Order
            {
                StoreNumber = model.StoreNumber,
                OrderNumber = model.OrderNumber,
                OrderDateTime = model.OrderDateTime,
                TotalPrice = model.TotalPrice,
                OrderDetails = model.Flavours.Select(f =>
                {
                    var flavour = flavours.FirstOrDefault(fdb => fdb.Id == f.FlavourId);

                    var amountOfIce = f.AmountOfIce;
                    if (flavour != null && flavour.AmountOfIce.HasValue)
                    {
                        amountOfIce = flavour.AmountOfIce.Value;
                    }

                    var detail = new OrderDetail
                    {
                        AmountOfIce = amountOfIce,
                        FlavourId = f.FlavourId,
                        Quantity = f.Quantity,
                        OrderToppingDetails = f.ToppingIds.Select(tid => new OrderToppingDetail { ToppingId = tid }).ToList()
                    };

                    return detail;
                }).ToList()
            };

            await _unitOfWork.OrderRepository.InsertAsync(order);
            await _unitOfWork.SaveChangesAsync();

            return errors;
        }

        public async Task<IEnumerable<Order>> GetOrderByNumber(string orderNumber)
        {
            return await _unitOfWork.OrderRepository.FilterAsync(o => o.OrderNumber == orderNumber);
        }

        public async Task<IEnumerable<Order>> GetOrders()
        {
            var orders = await _unitOfWork.OrderRepository.GetListAsync();
            return orders;
        }

        public async Task<IEnumerable<OrderReportModel>> GetReport(int month, int year)
        {
            var startDate = new DateTime(year, month, 1, 0, 0, 0, DateTimeKind.Utc);
            var endDate = startDate.AddMonths(1);

            return await _unitOfWork.OrderRepository.GetReportAsync(startDate, endDate);
        }
    }
}
