﻿using BubbleTea.Core.ApiModels;
using BubbleTea.DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BubbleTea.Service.Interfaces
{
    public interface IOrderService
    {
        Task<IEnumerable<string>> CreateOrder(OrderCreationModel model);
        Task<IEnumerable<Order>> GetOrders();
        Task<IEnumerable<Order>> GetOrderByNumber(string orderNumber);
        Task<IEnumerable<OrderReportModel>> GetReport(int month, int year);
    }
}
