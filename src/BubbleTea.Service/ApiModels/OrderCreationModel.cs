﻿using BubbleTea.DataAccess.Models;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using BubbleTea.Core.Enums;

namespace BubbleTea.Service.ApiModels
{
    public class OrderCreationModel
    {
        [Required, StringLength(50)]
        public string StoreNumber { get; set; }

        [Required, StringLength(50)]
        public string OrderNumber { get; set; }

        public DateTime OrderDateTime { get; set; } = DateTime.UtcNow;

        public decimal TotalPrice { get; set; }

        [Required]
        public List<OrderFlavourModel> Flavours { get; set; }
    }

    public class OrderFlavourModel
    {
        [Required]
        public int FlavourId { get; set; }

        public IceAmountType AmountOfIce { get; set; } = IceAmountType.None;

        public int Quantity { get; set; } = 1;

        public List<int> ToppingIds { get; set; }
    }
}
