using BubbleTea.Core.ApiModels;
using BubbleTea.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BubbleTea.Api.Controllers;

[ApiController]
public class OrdersController : ControllerBase
{
    private readonly IOrderService _orderService;

    public OrdersController(IOrderService orderService)
    {
        _orderService = orderService;
    }

    [HttpPost]
    [Route("api/orders")]
    public async Task<IActionResult> Create(OrderCreationModel model)
    {
        var errors = await _orderService.CreateOrder(model);
        if (errors.Any()) return BadRequest(errors);

        return Ok();
    }

    [HttpGet]
    [Route("api/orders/report")]
    public async Task<IActionResult> Report(string monthYear)
    {
        int month = DateTime.Now.Month;
        int year = DateTime.Now.Year;

        if (string.IsNullOrWhiteSpace(monthYear) || !monthYear.Contains("-"))
        {
            return BadRequest("Invalid parameter");
        }
        var timeParts = monthYear.Split('-');
        if (timeParts.Length != 2 || !int.TryParse(timeParts[0], out year)
            || !int.TryParse(timeParts[1], out month) || month < 1 || month > 12)
        {
            return BadRequest("Invalid parameter");
        }

        return Ok(new
        {
            stores = await _orderService.GetReport(month, year)
        });
    }
}
