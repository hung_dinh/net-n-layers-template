using BubbleTea.Core.ApiModels;
using BubbleTea.DataAccess;
using BubbleTea.DataAccess.DbContexts;
using BubbleTea.DataAccess.Implementation;
using BubbleTea.DataAccess.Models;
using BubbleTea.Service.Implementation;
using BubbleTea.Service.Interfaces;

namespace BubbleTea.Service.Tests
{
    public class UnitTests_Insert_Orders
    {
        private IOrderService _orderService;

        [SetUp]
        public void Setup()
        {
            var dbContext = new BubbleTeaDbContext();
            var orderRepository = new OrderRepository(dbContext);
            var orderDetailRepository = new Repository<OrderDetail>(dbContext);
            var flavourRepository = new Repository<Flavour>(dbContext);
            var toppingRepository = new Repository<Topping>(dbContext);
            var flavourToppingRepository = new Repository<FlavourTopping>(dbContext);
            var orderToppingDetailRepository = new Repository<OrderToppingDetail>(dbContext);
            var unitOfWork = new UnitOfWork(dbContext, orderRepository, orderDetailRepository, orderToppingDetailRepository,
                flavourRepository, toppingRepository, flavourToppingRepository);
            _orderService = new OrderService(unitOfWork);

            SeedingData.Initiate(dbContext);
        }

        [Test]
        public async Task Test_Insert_Order()
        {
            var orderCreationModel = new OrderCreationModel
            {
                StoreNumber = "Store001",
                OrderNumber = "Order001",
                OrderDateTime = DateTime.Now.AddDays(-3),
                TotalPrice = 200,
                Flavours = new List<OrderFlavourModel> {
                    new OrderFlavourModel
                    {
                        FlavourId = 1,
                        AmountOfIce = Core.Enums.IceAmountType.Half,
                        Quantity = 1,
                        ToppingIds = new List<int> { 1, 2 },
                    }
                }
            };

            try
            {
                await _orderService.CreateOrder(orderCreationModel);
                var createdOrders = await _orderService.GetOrderByNumber(orderCreationModel.OrderNumber);
                if (createdOrders == null || createdOrders.Count() == 0)
                {
                    Assert.Fail();
                }
            }
            catch (Exception e)
            {
                Assert.Fail();
            }

            Assert.Pass();
        }
    }
}