using BubbleTea.Core.ApiModels;
using BubbleTea.DataAccess;
using BubbleTea.DataAccess.DbContexts;
using BubbleTea.DataAccess.Implementation;
using BubbleTea.DataAccess.Models;
using BubbleTea.Service.Implementation;
using BubbleTea.Service.Interfaces;

namespace BubbleTea.Service.Tests
{
    public class UnitTests_Get_Report
    {
        private IOrderService _orderService;

        [SetUp]
        public void Setup()
        {
            var dbContext = new BubbleTeaDbContext();
            var orderRepository = new OrderRepository(dbContext);
            var orderDetailRepository = new Repository<OrderDetail>(dbContext);
            var flavourRepository = new Repository<Flavour>(dbContext);
            var toppingRepository = new Repository<Topping>(dbContext);
            var flavourToppingRepository = new Repository<FlavourTopping>(dbContext);
            var orderToppingDetailRepository = new Repository<OrderToppingDetail>(dbContext);
            var unitOfWork = new UnitOfWork(dbContext, orderRepository, orderDetailRepository, orderToppingDetailRepository,
                flavourRepository, toppingRepository, flavourToppingRepository);
            _orderService = new OrderService(unitOfWork);

            SeedingData.Initiate(dbContext);
        }

        [Test]
        public async Task Test_Insert_Order()
        {
            try
            {
                await _orderService.CreateOrder(new OrderCreationModel
                {
                    StoreNumber = "Store001",
                    OrderNumber = "Order001",
                    OrderDateTime = DateTime.Now.AddDays(-3),
                    TotalPrice = 200,
                    Flavours = new List<OrderFlavourModel> {
                    new OrderFlavourModel
                    {
                        FlavourId = 1,
                        AmountOfIce = Core.Enums.IceAmountType.Half,
                        Quantity = 1,
                        ToppingIds = new List<int> { 1, 2 },
                    }
                }
                });

                await _orderService.CreateOrder(new OrderCreationModel
                {
                    StoreNumber = "Store001",
                    OrderNumber = "Order002",
                    OrderDateTime = DateTime.Now.AddDays(-3),
                    TotalPrice = 300,
                    Flavours = new List<OrderFlavourModel> {
                    new OrderFlavourModel
                    {
                        FlavourId = 2,
                        AmountOfIce = Core.Enums.IceAmountType.Full,
                        Quantity = 1,
                        ToppingIds = new List<int> { 2 },
                    }
                }
                });

                await _orderService.CreateOrder(new OrderCreationModel
                {
                    StoreNumber = "Store002",
                    OrderNumber = "Order003",
                    OrderDateTime = DateTime.Now.AddDays(-3),
                    TotalPrice = 450,
                    Flavours = new List<OrderFlavourModel> {
                    new OrderFlavourModel
                    {
                        FlavourId = 2,
                        AmountOfIce = Core.Enums.IceAmountType.Half,
                        Quantity = 1,
                        ToppingIds = new List<int> {  },
                    }
                }
                });

                var report = await _orderService.GetReport(month: 6, year: 2023);
                if (report == null || report.Count() != 2)
                {
                    Assert.Fail();
                }
                if (report.First().StoreNumer != "Store001"
                    || report.First().OrderPriceSum != 500
                    || report.First().OrderTotal != 2
                    || report.Last().StoreNumer != "Store002"
                    || report.Last().OrderPriceSum != 450
                    || report.Last().OrderTotal != 1)
                {
                    Assert.Fail();
                }
            }
            catch (Exception e)
            {
                Assert.Fail();
            }

            Assert.Pass();
        }
    }
}